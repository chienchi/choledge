This is an application for REAL-TIME DECISION SUPPORT TOOLS for Cholera Epidemiology.

See more info and usage at [Docker hub](https://hub.docker.com/r/bioedge/choledge)

[Online example](https://choledge.herokuapp.com/Cholera)
<!-- (https://bioedge.lanl.gov/nextstrain/Cholera)-->

![screenshot](https://gitlab.com/chienchi/choledge/raw/23beaab11ff8b32ebf180eefdfd456f97b13d79a/images/cholera.png)
