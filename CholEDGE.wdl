task qc {
	File mappingfile
        Int cpu
	String indir
	String outdir
	String qsub
	Int mem = 20

	command {
		if [ ! -f "${outdir}/QC/snippy_input.tab" ]
		then
			if ${qsub} ; then
				batch_FaQCs.py -m ${mappingfile} -d ${indir} -o ${outdir}/QC -c ${cpu} --min_L 30 --uge
			else
				batch_FaQCs.py -m ${mappingfile} -d ${indir} -o ${outdir}/QC -c ${cpu} --min_L 30
			fi
		fi
		mv -f ${outdir}/QC/sample_metadata* ${outdir}/ 2>/dev/null || true
	} 
	output {
		# do not use read_string. it will make hardlink to files
		String out_file="${outdir}/QC/snippy_input.tab"
	}

	runtime { memory: mem + "GB"
                  cpu: cpu }
}

task snippy {
        File snippy_input_tab
        File ref = "/db/CholEDGEdb/ref/ref.fa"
        Int cpu
        String outdir2
	String qsub = 'false'
	String qsubMem = '20GB'
        String db = "/db/CholEDGEdb"
        command {
		# triple angle braces use ~ for evaluate WDL varialbe 
		mkdir -p ${outdir2}/snippy ${outdir2}/core
		ln -s ${db} ${outdir2}/CholEDGEdb
                snippy-multi ${snippy_input_tab} --ref ${ref} --cpus ${cpu} --mincov 3 --tmpdir ${outdir2}/snippy > ${outdir2}/snippy/runsnippy_orig.sh
		cp ${outdir2}/snippy/runsnippy_orig.sh ${outdir2}/snippy/runsnippy.sh
		cd "${outdir2}/snippy"
		

		##adding force, generate snippy-only.sh and snippy_core.sh
		update_snippy_cmd.sh ${snippy_input_tab} runsnippy.sh 

		if ${qsub} ; then
			uge_job_runner.py -l snippy-only.sh --jobname snippy --mem ${qsubMem} --cpu ${cpu} --qlog snippy_qsub.log --qwait
		else
			bash snippy-only.sh
		fi
                
		## checking number of snps.vcf files and remove sample from run snippy-core command if no .vcf file 
		numSnippyRun=`cat snippy-only.sh | wc -l`
		numVcfFile=`ls */snps.vcf | wc -l`
		echo "numVcfFile $numVcfFile"
		echo "numSnippyRun $numSnippyRun"
		if [ $numSnippyRun -ne $numVcfFile ]; then
			for i in * ; do 
				if [ -d $i ] && [ ! -f "$i/snps.vcf" ]; then 
					sed -i "s/$i//" snippy-core.sh
					echo "snippy failed on sample $i"
				fi
			done
		fi
		
		cd ${outdir2}/core
		if [ ! -s "${outdir2}/core/core.full.aln" ]
		then
			bash snippy_core.sh
                	snippy-clean_full_aln  ${outdir2}/core/core.full.aln > ${outdir2}/core/clean.full.aln
		else
			if [ ! -s "${outdir2}/core/clean.full.aln" ]
			then
                		snippy-clean_full_aln  ${outdir2}/core/core.full.aln > ${outdir2}/core/clean.full.aln
			fi
		fi
        }
        output {
		
                String clean_aln = "${outdir2}/core/clean.full.aln"
        }
        runtime { memory: qsubMem
		  cpu: cpu }
}

task gubbins {
        String clean_aln_file
        Int cpu
        String outdir3
	Int mem = 50
        command {
                cd ${outdir3}
		mkdir -p gubbins && cd gubbins
                run_gubbins.py --threads ${cpu} -p gubbins  ${clean_aln_file} 
                snp-sites gubbins.filtered_polymorphic_sites.fasta > clean.core.aln
		rm -rf tmp*
                #snp-sites -v gubbins.filtered_polymorphic_sites.fasta > clean.core.vcf
                #FastTree -gtr -nt clean.core.aln > clean.core.tree
        } 
        output {
                String out_aln = "${outdir3}/gubbins/clean.core.aln"
        }
        runtime { memory: mem + "GB" 
		  cpu: cpu }
}

task nextstrain {
	String clean_core_aln
        Int cpu
        String outdir4
	String tree = 'raxml'
	Int mem = 20

	command {
		tmpl_dir=$(dirname $(which generateMetadata.pl))
		generateMetadata.pl -n Cholera -tree ${tree} -o ${outdir4}/auspice_workflow -aln ${clean_core_aln} -conf $tmpl_dir/config.tmpl -snake $tmpl_dir/Snakefile.tmpl -d ${outdir4} -map ${outdir4}/CholEDGEdb/metadata.tsv
		cd  ${outdir4}/auspice_workflow 
		snakemake -j ${cpu}
		if [ -d "/auspice/data" ] then;
			projnam=$(basename ${outdir4})
			cp -f ${outdir4}/auspice_workflow/auspice/Cholera.json /auspice/data/$projnam_Cholera.json || true
		fi
	}
	output {
		String auspice_json = "${outdir4}/auspice_workflow/auspice/Cholera.json"
	}
	runtime { memory: mem + "GB"
		  cpu:cpu}

}

workflow core_snps {
        String projdir
        Int cpu
        String qsub = 'false'
	Boolean DoQC=true
	Boolean DoNextStrain=true
	File? snippy_input_file

	if (DoQC){
        	call qc {
			input:  cpu = cpu,
			outdir = projdir,
			qsub = qsub
		}
        }
        call snippy {
                input:  snippy_input_tab = if (DoQC) then qc.out_file else snippy_input_file,
                cpu = cpu,
                outdir2 = projdir,
		qsub = qsub
        }
        call gubbins {
               input:  clean_aln_file = snippy.clean_aln,
                cpu =cpu,
                outdir3 = projdir
        }
	if (DoNextStrain) {
		call nextstrain {
			input: clean_core_aln = gubbins.out_aln,
			cpu = cpu,
			outdir4 = projdir
		}
	}
}
