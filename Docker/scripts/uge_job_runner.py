#!/usr/bin/env python3
__version__ = "0.1.0"

import sys, os, time, argparse
import ugetk
import logging
import log

class SmartFormatter(argparse.HelpFormatter):
    def _split_lines(self, text, width):
            if text.startswith('R|'):
                return text[2:].splitlines()
            # this is the RawTextHelpFormatter._split_lines
            return argparse.HelpFormatter._split_lines(self, text, width)

def setup_argparse():
    parser = argparse.ArgumentParser(prog='uge_job_runner.py',
        description = '''Script to submit list commands to uge cluster''',
        formatter_class = SmartFormatter)

    inGrp = parser.add_argument_group('Required Input')
    inGrp.add_argument('-l', '--list', required=True, type=str,help='List files with independent commands per line')

    optGrp = parser.add_argument_group('Parameters')
    optGrp.add_argument('--jobname' ,type=str, help='-N JobName')
    optGrp.add_argument('--mem', type=str,  default='10G', help='-l h_vmem= mem,mem_free=mem [default 10G]')
    optGrp.add_argument('--cpu', type=int, metavar='<INT>', default=8, help='-pe smp cpu [default 8]')
    optGrp.add_argument('--qlog', type=str, default=None, help='-o ')
    optGrp.add_argument('--email', type=str, default=None, help='-M ')
    optGrp.add_argument('--qwait', action='store_true', help='Wait all jobs finished.')

    parser.add_argument('--version', action='version', version='%(prog)s v{version}'.format(version=__version__))
    #parser.add_argument('--quiet', action='store_true', help='Keep messages in terminal minimal')
    parser.add_argument('--verbose', action='store_true', help='Show more infomration in log')
    return parser.parse_args()

def get_runtime(start):
    end = time.time()
    hours, rem = divmod(float(end-start), 3600)
    minutes, seconds = divmod(rem, 60)
    runtime ="Running time: {:0>2}:{:0>2}:{:0>2}".format(int(hours),int(minutes), int(seconds))
    return runtime

if __name__ == '__main__':
    argvs    = setup_argparse()
    begin_t  = time.time()
    runnerlogger = logging.getLogger(__name__)
    log.log_init(runnerlogger,verbose=argvs.verbose)

    jobids = []
    with open (argvs.list,'r') as f:
        for line in f:
            cmd = line.rstrip().split()
            runnerlogger.debug(cmd)
            jid = ugetk.qsub(cmd, argvs.jobname,argvs.mem,argvs.cpu,argvs.qlog,argvs.email)
            jobids.append(jid)

    if (argvs.qwait):
        runnerlogger.info("Wait for submitted %d jobs ..." % len(jobids))
        ugetk.qwait_all(jobids)

    
    totol_runtime = "Total %s" % get_runtime(begin_t)
    runnerlogger.info(totol_runtime)   
