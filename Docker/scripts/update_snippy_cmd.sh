#!/usr/bin/env bash

snippy_input_tab=$1
runsnippy_file=$2

for i in `awk '{print $1}' $snippy_input_tab`
do
	if [[ ! -f "$i/snps.aligned.fa" ]] && [[ ! -f "$i/snps.vcf" ]]
        then
		sed -i "s;--outdir '$i';--outdir '$i' --force;" $runsnippy_file

        fi
done

grep -v 'snippy-core' runsnippy.sh > snippy-only.sh


if [ -f "../CholEDGEdb/snippy-core-exclude.sh" ]
then
	grep 'snippy-core' $runsnippy_file | awk '{$1=""; $2=""; $3=""; print $0}' | sed -re 's/(\S+)/\.\.\/snippy\/\1/g' | cat ../CholEDGEdb/snippy-core-exclude.sh - |  tr '\n' ' ' > ../core/snippy_core.sh
else
	grep 'snippy-core' $runsnippy_file   > ../core/snippy_core.sh
fi
