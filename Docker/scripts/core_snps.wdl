task qc {
	File mappingfile
        Int cpu
	String indir
	String outdir
	String qsub

	command {
		export PATH=/panfs/biopan01/edge-dev-master/edge-dev-test/bin:$PATH:/users/andylo/scratch/projects/CholEDGE/scripts
		if [ ! -f "${outdir}/QC/snippy_input.tab" ]
		then
			if ${qsub} ; then
				batch_FaQCs.py -m ${mappingfile} -d ${indir} -o ${outdir}/QC -c ${cpu} --min_L 30 --uge
			else
				batch_FaQCs.py -m ${mappingfile} -d ${indir} -o ${outdir}/QC -c ${cpu} --min_L 30
			fi
		fi
	} 
	output {
		# do not use read_string. it will make hardlink to files
		String out_file="${outdir}/QC/snippy_input.tab"
	}

	runtime { memory: "20 GB"
                  cpu: cpu }
}

task snippy {
        File snippy_input_tab
        File ref
        Int cpu
        String outdir2
	String qsub
	String qsubMem
        command {
		# triple angle braces use ~ for evaluate WDL varialbe 
                source /panfs/biopan01/edge-dev-master/edge-dev-test/thirdParty/Anaconda3/bin/activate /panfs/biopan01/edge-dev-master/edge-dev-test/thirdParty/Anaconda3/envs/choledge
		export PATH=$PATH:/users/andylo/scratch/projects/CholEDGE/scripts
		mkdir -p ${outdir2}/snippy
                snippy-multi ${snippy_input_tab} --ref ${ref} --cpus ${cpu} --mincov 3 --tmpdir ${outdir2}/snippy > ${outdir2}/snippy/runsnippy_orig.sh
		cp ${outdir2}/snippy/runsnippy_orig.sh ${outdir2}/snippy/runsnippy.sh
		cd "${outdir2}/snippy"
		##adding force
		
		update_snippy_cmd.sh ${snippy_input_tab} runsnippy.sh 
		grep -v 'snippy-core' runsnippy.sh > snippy-only.sh
		grep 'snippy-core' runsnippy.sh > snippy-core.sh
		sed -i "s/snippy-core /snippy-core --mask auto /" snippy-core.sh
		if ${qsub} ; then
			uge_job_runner.py -l snippy-only.sh --jobname snippy --mem ${qsubMem} --cpu ${cpu} --qlog snippy_qsub.log --qwait
		else
			bash snippy-only.sh
		fi
                
		## checking number of snps.vcf files and remove sample from run snippy-core command if no .vcf file 
		numSnippyRun=`cat snippy-only.sh | wc -l`
		numVcfFile=`ls */snps.vcf | wc -l`
		echo "numVcfFile $numVcfFile"
		echo "numSnippyRun $numSnippyRun"
		if [ $numSnippyRun -ne $numVcfFile ]; then
			for i in * ; do 
				if [ -d $i ] && [ ! -f "$i/snps.vcf" ]; then 
					sed -i "s/$i//" snippy-core.sh
					echo "snippy failed on sample $i"
				fi
			done
		fi

		bash snippy-core.sh
                snippy-clean_full_aln  ${outdir2}/snippy/core.full.aln > ${outdir2}/snippy/clean.full.aln
        }
        output {
		
                String clean_aln = "${outdir2}/snippy/clean.full.aln"
        }
        runtime { memory: "20 GB"
		  cpu: cpu }
}

task gubbins {
        File clean_aln_file
        Int cpu
        String outdir3
        command {
                source /panfs/biopan01/edge-dev-master/edge-dev-test/thirdParty/Anaconda3/bin/activate /panfs/biopan01/edge-dev-master/edge-dev-test/thirdParty/Anaconda3/envs/choledge
                cd ${outdir3}
		mkdir -p gubbins && cd gubbins
                run_gubbins.py --threads ${cpu} -p gubbins  ${clean_aln_file} 
                snp-sites gubbins.filtered_polymorphic_sites.fasta > clean.core.aln
                #snp-sites -v gubbins.filtered_polymorphic_sites.fasta > clean.core.vcf
                #FastTree -gtr -nt clean.core.aln > clean.core.tree
        } 
        output {
                String out_aln = "${outdir3}/clean.core.aln"
        }
        runtime { memory: "50 GB" 
		  cpu: cpu }
}

task nextstrain {
	File clean_core_aln
        Int cpu
        String outdir3

	command {
                source /panfs/biopan01/edge-dev-master/edge-dev-test/thirdParty/Anaconda3/bin/activate /panfs/biopan01/edge-dev-master/edge-dev-test/thirdParty/Anaconda3/envs/choledge
		
	}
	output {
	}
	runtime { memory: "10GB"
		  cpu:cpu}

}

workflow core_snps {
        String projdir
        Int cpu
        String qsub
        call qc {
               input:  cpu = cpu,
                outdir = projdir,
		qsub = qsub
        }
        call snippy {
               input:  snippy_input_tab = qc.out_file,
                cpu = cpu,
                outdir2 = projdir,
		qsub = qsub
        }
        call gubbins {
               input:  clean_aln_file = snippy.clean_aln,
                cpu =cpu,
                outdir3 = projdir
        }
}
