#!/usr/bin/env perl

use strict;
use File::Spec;
use Getopt::Long;
use JSON;
use LWP::UserAgent;
use Color::Mix;
use FindBin qw($RealBin);
use Data::Dumper;

$| = 1;

my ($virus, $outDir, $inAln, $configTmpl, $snakeTmpl, $inMap, $dataDir, $htmlColors, $wrongLatlng, $metaData);
my $tree_method = 'raxml';
GetOptions(
	'n=s',  \$virus,	
	'o=s',  \$outDir,
	'aln=s',  \$inAln,		
    'conf=s', \$configTmpl,		
    'snake=s', \$snakeTmpl,			
	'map=s',  \$inMap,			
	'd=s',  \$dataDir,	
    'colors=s',  \$htmlColors,
    'wrongLatlng', \$wrongLatlng,
    'tree=s', \$tree_method,
);

usage() unless $virus && $outDir && $inAln;

my $continentsJson="$RealBin/../geo/continents.json";
my $countriesJson="$RealBin/../geo/countries.json";
my $ccJson="$RealBin/../geo/country-and-continent-codes-list-csv_json.json";



$configTmpl = "./config.tmpl" unless $configTmpl;
$snakeTmpl = "./snake.tmpl" unless $snakeTmpl;

my $ua = LWP::UserAgent->new(); 
$ua->proxy('https', 'http://proxyout.lanl.gov:8080');

my $color = Color::Mix->new();

my @colors = ();
if($htmlColors) {
    @colors = ();
    open IN, "$htmlColors";
    while(<IN>) {
        chomp;
        push(@colors, $_);
    }
    close(IN);
}

my $continentsCollection = load_collection(readListFromJson($continentsJson));
my $countriesCollection = load_collection(readListFromJson($countriesJson));
my $ccCollection = load_collection(readListFromJson($ccJson));

#print Dumper $countriesCollection;
#print Dumper $continentsCollection;
#print Dumper $ccCollection;
#exit;
#my $sra_url = "https://www.ebi.ac.uk/ena/data/view";
my $sra_url = "https://www.ebi.ac.uk/ena/browser/view";
my $ncbi_url = "https://www.ncbi.nlm.nih.gov/nuccore";
$dataDir = "/edge_bsve/edge_ui/EDGE_output_aido_ecoli" unless $dataDir;
#my %map = ();
my %countriesMap = ();
my %continentsMap = ();
#my %ccodeMap = ();

# setup output file structure
my $inAln_fullpath = File::Spec->rel2abs($inAln);
my $aln = "$outDir/data/sequences.fasta";
my $metadataTsv = "$outDir/data/metadata.tsv";
my $droppedStrainsTxt = "$outDir/config/dropped_strains.txt";
my $latlngTsv = "$outDir/config/lat_longs.tsv";
my $colorTsv = "$outDir/config/colors.tsv";
my $configJson = "$outDir/config/auspice_config.json";
my $snakefile = "$outDir/Snakefile";

`mkdir -p $outDir`;
`mkdir -p $outDir/data`;
`mkdir -p $outDir/config`;
`mkdir -p $outDir/auspice`;
`mkdir -p $outDir/results`;

`touch $droppedStrainsTxt`;

# get meta data mapping info
# this is metedata file from Daryl which may need fixed column/format for other pathogen
my %idMap=();
if($inMap) {
    my @parts;
    open(MAP, "$inMap");
    while(<MAP>) {
        chomp;
        @parts = split(/\t/);
        $parts[0] =~ s/#/_/g;
        my $country = $parts[2];
        # fix Guinea-Bissau
        $country =~ s/Guinea.Bissau/Guinea-Bissau/;
        $country =~ s/Gaza/Palestine/;
        $country =~ s/Kurdistan/Iraq/;
	my $cca2 = $countriesCollection->{$country}->{cca2};
        my $continent = $ccCollection->{$cca2}->{'Continent_Name'};
        my $lat = $parts[4];
        my $lng = $parts[3];
        $idMap{"$parts[0]"}->{lat} = $lat; 
        $idMap{"$parts[0]"}->{lng} = $lng; 
        $idMap{"$parts[0]"}->{virus} = $virus; 
        $idMap{"$parts[0]"}->{strain} = "$parts[0]";
        $idMap{"$parts[0]"}->{date} =  ($parts[5])?"$parts[5]-XX-XX":"Unknown"; 
        $idMap{"$parts[0]"}->{country} = $country; 
        $idMap{"$parts[0]"}->{continent} = $continent; 
        $idMap{"$parts[0]"}->{accession} = $parts[6];
	$idMap{"$parts[0]"}->{author} = ($parts[17])? $parts[17]:"Unknown";
	$idMap{"$parts[0]"}->{wave} = ($parts[14])?$parts[14]:"Unknown";;
	$idMap{"$parts[0]"}->{transmissionEvent} = ($parts[9])?$parts[9]:"Unknown";;
	$idMap{"$parts[0]"}->{source} = $parts[7];
	$idMap{"$parts[0]"}->{serotype} = $parts[8];
        if ($parts[6] =~ /[A-Z]{3}\d+/){
            $idMap{"$parts[0]"}->{url} = "$sra_url/$parts[6]";
        }else{
            #$parts[6] =~ s/_/ /;
            $idMap{"$parts[0]"}->{url} = "$ncbi_url/$parts[6]";
        }
    }
    close(MAP);
}

# find all strains in the input alignment
open(IN, "<", "$inAln_fullpath") or die "Cannot open $inAln_fullpath";
open(OUT, ">", "$aln") or die "Cannot write to $aln";
my @ids = ();
while(<IN>) {
    chomp;
    if(/^>(.*)/) {
	my $id = $1;
	$id =~ s/#/_/g;
        push(@ids, $id);
	print OUT ">$id\n";
    }else{
        print OUT "$_\n";
    }
}
close(IN);
close(OUT);
my $sra_meta;
# create configuration files for auspice
open OUTMETA, ">$metadataTsv";
#strain  virus   accession       date    region  country division        city    db      segment authors url     title   journal paper_url
print OUTMETA "strain\tvirus\tdate\tcontinent\tcountry\taccession\tsource\tserotype\tauthors\twave\ttransmission_event\turl\n";
foreach my $id (@ids) {
    next if(!$id);
    next if($id =~ /Reference/);
    my $strain = $id;
    if ($idMap{$id}){
        my $lat = $idMap{$id}->{lat};
        my $lng = $idMap{$id}->{lng};
        my $country = $idMap{$id}->{country};
        my $continent = $idMap{$id}->{continent} || "unknown";
        my $ccode = $countriesCollection->{$country}->{cca2};
	my $continent_code = $ccCollection->{$ccode}->{Continent_Code};
        $continent =~ s/\s+/_/g;
        $continent = lc($continent);
        $country =~ s/\s+/_/g;
        $country = lc($country);
        print OUTMETA $id,"\t",
                      $idMap{$id}->{virus},"\t",
                      $idMap{$id}->{date},"\t",
                      $continent,"\t",
                      $country,"\t",
                      $idMap{$id}->{accession},"\t",
                      $idMap{$id}->{source},"\t",
                      $idMap{$id}->{serotype},"\t",
                      $idMap{$id}->{author},"\t",
                      $idMap{$id}->{wave},"\t",
                      $idMap{$id}->{transmissionEvent},"\t",
                      $idMap{$id}->{url},"\n";
        #print $idMap{$id}->{author},"\t";
        my $continent_lat = $continentsCollection->{$continent_code}->{'lat'};
        my $continent_lng = $continentsCollection->{$continent_code}->{'lng'};
        #print "$country\n" if (!$continent);
        $continentsMap{$continent} = "continent\t$continent\t$continent_lat\t$continent_lng" if ($continent);
	$countriesMap{$country} = sprintf("country\t%s\t%.7f\t%.7f",$country,$lat,$lng);
    }else{
        #print "$id\n";
        $sra_meta = $dataDir."/$id/sra_metadata_sample.txt";
        my $input_meta = $dataDir."/sample_metadata.json";
        if(!-e $sra_meta) {
            #warn "$sra_meta not found\n";
            # print "dropped $strain\n";
            #`echo $strain >> $droppedStrainsTxt`;
            print OUTMETA "$id\t$virus\tunknown\tunknown\tunknown\t$id\tunknown\tunknown\tunknown\tunknown\tunknown\tunknown\n";
        }else{
            my $print_string = parseMeta($strain, $sra_meta, $input_meta, $id);
            print OUTMETA $print_string,"\n";
        }
    }
}
close(OUTMETA);

# generate color sets
my %continentColors = (
    "africa" => "#ee8a29",
    "antarcitca" => "#008000",
    "asia" => "#5ba793",
    "europe" => "#559FFF",
    "north_america" => "#912481",
    "oceania" => "#D2B48C",
    "south_america" => "#912481",
    "america" => "#912481",
    "unknown" => '#B0B0B0'
);
# create config files
# create lat_lngs.tsv and color.tsv
open LATLNG, ">$latlngTsv";
open my $COLOR_fh, ">$colorTsv";
foreach my $key (sort (keys %continentsMap)) {
    if ($key =~/unknown/i){
	print LATLNG "continent\t$key\t0\t0\n";
    }else{
    	print LATLNG $continentsMap{$key}."\n";
    }
    print $COLOR_fh "continent\t$key\t".$continentColors{$key}."\n";
}
print LATLNG "continent\tamericas\t-16.2901540\t-63.5886530\n";

my $size = keys %countriesMap;
my $aref = generateColors($size);
my @countryColors = @$aref;
my $i = 0;
foreach my $key (sort (keys %countriesMap)) {
    if ($key =~/unknown/i){
        print LATLNG "country\t$key\t0\t0\n";
    	print $COLOR_fh "country\t$key\t"."#B0B0B0"."\n";
    }else{
        print LATLNG $countriesMap{$key}."\n";
    	print $COLOR_fh "country\t$key\t#".$countryColors[$i]."\n";
    }
    $i ++;
}
&printOtherColor($COLOR_fh);

close(LATLNG);
close($COLOR_fh);

# create auspice config json
my $configContent = do {
    local $/ = undef;
    open my $fh, "<", $configTmpl
        or die "could not open $configTmpl: $!";
    <$fh>;
};

$configContent =~ s/<VIRUS>/$virus/g;
open CONF, ">$configJson";
print CONF $configContent;
close(CONF);

# create Snakefile
my $snakeContent = do {
    local $/ = undef;
    open my $fh, "<", $snakeTmpl
        or die "could not open $snakeTmpl: $!";
    <$fh>;
};

$snakeContent =~ s/<VIRUS>/$virus/g;
$snakeContent =~ s/<TreeMethod>/$tree_method/g;
open SNAKE, ">$snakefile";
print SNAKE $snakeContent;
close(SNAKE);

#######################
sub load_collection{
    my $Collection = shift;
    my @array = @$Collection;
    my %hash;
    foreach my $r (@array){
        my $name = (ref($r->{"name"}) eq "HASH" )? $r->{"name"}->{common}: $r->{"name"} || $r->{Country_Name};
        my $region = $r->{"region"};
        my $subregion = $r->{"subregion"};
        my $cca2 = $r->{"cca2"} || $r->{code} || $r->{Two_Letter_Country_Code}; 
	my $lat =  $r->{lat} || $r->{"latlng"}->[0];
	my $lng =  $r->{lon} || $r->{"latlng"}->[1];
        my $Continent_Code = $r->{Continent_Code};
        my $Continent_Name = $r->{Continent_Name};
        if (ref($r->{"name"}) eq "HASH"){
	    $hash{$name}->{cca2} = $cca2;
            my $official = $r->{"name"}->{official};
            $hash{$official}->{cca2} = $cca2;
            my $alt = $r->{"name"}->{altSpellings};
            $hash{$alt}->{cca2} = $cca2;
        }
        $hash{$cca2}->{name} = $name;
        $hash{$cca2}->{region} = $region if ($region);
        $hash{$cca2}->{subregion} = $subregion if ($subregion);
        $hash{$cca2}->{lat} = $lat if ($lat);
        $hash{$cca2}->{lng} = $lng if ($lng);
        $hash{$cca2}->{Continent_Code} = $Continent_Code if ($Continent_Code);
        $hash{$cca2}->{Continent_Name} = $Continent_Name if ($Continent_Name);
    }
    return \%hash;	
} 
sub parseMeta {
    my $strain = shift;
    my $meta = shift;
    my $meta2 = shift;
    my $acc = shift;
    my ($cdate, $lat, $lng, $sacc, $scenter, $stitle, $country);
    if ( -e $meta){
       open META, "$meta";
       while(<META>) {
           if(/collection_date=(.*)/) {
               $cdate= $1;
           }
           if(/lat=(.*)/ && !$lat) {
               $lat= $1;
           }
           if(/lng=(.*)/ && !$lng) {
               $lng= $1;
           }
           if(/center_name=(.*)/) {
               $scenter= $1;
           }
           if(/sample_accession=(.*)/) {
               $sacc= $1;
           }
           if(/sample_title=(.*)/) {
               $stitle = $1;
           }
           if(/country=(.*)/){
               $country = $1;
           }
        }
        close META;
    }
    my $alt = $acc;
    if ( -e $meta2){
        my $meta_list = readListFromJson($meta2);
	$alt =~ s/^s_//g;
        #SampleID       Files   Country Years
        $cdate = $meta_list->{$acc}->{Years} || $meta_list->{$alt}->{Years} ;
        $country = $meta_list->{$acc}->{Country} || $meta_list->{$alt}->{Country};
	
    }
    $scenter = "unknown" unless $scenter;
    $cdate = 'unknown' unless $cdate;
    if($wrongLatlng) {
        my $tmp = $lat;
        $lat = $lng;
        $lng = $tmp;
    }

    print "$cdate, $lat, $lng\n";
    my $latlng = "$lat,$lng";
    my ($state, $ccode);
    #if($map{$latlng}) {
    #    ($state, $country, $ccode) = split(/\t/, $map{$latlng});
    #} else {
        #($state, $country, $ccode) = getAddress($lat,$lng);
    #    $map{$latlng} = "$state\t$country\t$ccode";
    #}
    my $continent;
    if(! $country) {
        
        # add to dropped_strains.txt
        #print "dropped $strain\n";
        #`echo $strain >> $droppedStrainsTxt`;
    } else {
        # find country offical name, continent name
        $continent = findContinent($country);
	
        # print to metadata.tsv
    }
    my $source="unknown";
    my $serotype="unknown";
    my $wave="unknown";
    my $transmissionEvent= "unknown";
#print OUTMETA "strain\tvirus\tdate\tcontinent\tcountry\taccession\tsource\tserotype\tauthor\nurl\n";
    my $string = "$strain\t$virus\t$cdate\t$continent\t$country\t$sacc\t$source\t$serotype\t$scenter\t$wave\t$transmissionEvent\t$sra_url/$sacc";
    return $string;
    #sleep(1);#avoid map API limit/second requests
}

sub getAddress {
    # not used
    my $lat = shift;
    my $lng = shift;

    if(!$lat || !$lng) {
        die "ERROR: lat: $lat, lng: $lng\n";
    }
        
    my $geocodeapi = "https://us1.locationiq.com/v1/reverse.php?key=68e0fb0816235e&format=json";
    my $url = $geocodeapi . "&lat=" . $lat."&lon=".$lng;  
    my $res = $ua->get($url);
    my $json = $res->decoded_content;
    print "$url\n";
    #print "json: $json\n";
    my $d_json = decode_json( $json );
    if($d_json->{error}) {
        die "ERROR: ".$d_json->{error};
    }
    my $address = $d_json->{display_name};
    my ($ccode, $state, $country);
    if($d_json->{address}->{country_code}){
        $ccode = $d_json->{address}->{country_code};   
        $state =  $d_json->{address}->{state}; 
        $country = $d_json->{address}->{country}; 
    } else {
        $ccode = "unknown";
        $state =  "unknown"; 
        $country = "unknown"; 
    }         

    print "$state, $country, $ccode\n";
    return ($state, $country, $ccode);
}

sub findContinent {
    my $country = shift;
    #if($ccodeMap{$ccode}) {
    #    return ($ccodeMap{$ccode}[0], $ccodeMap{$ccode}[1]);
    #}
    # find country commom name
    #my $data; 
    my $ccode = $countriesCollection->{$country}->{cca2};
    if (!$ccode){
        return "unknown";
    }
    $country =~ s/\s+/_/g;
    $country = lc($country);
    my $lat = sprintf("%.7f",$countriesCollection->{$ccode}->{lat});
    my $lng = sprintf("%.7f",$countriesCollection->{$ccode}->{lng});
    $countriesMap{$country} = "country\t$country\t$lat\t$lng";
    # find continent
    my $continent = $ccCollection->{$ccode}->{'Continent_Name'};
    my $continent_code = $ccCollection->{$ccode}->{Continent_Code};
    $continent =~ s/\s+/_/g;
    $continent = lc($continent);
    $lat = $continentsCollection->{$continent_code}->{'lat'};
    $lng = $continentsCollection->{$continent_code}->{'lng'};
    # add continent name with lat/lng to a hash map
    $continentsMap{$continent} = "continent\t$continent\t$lat\t$lng" if ($continent);
    #$ccodeMap{$ccode}[0] = $country;
    #$ccodeMap{$ccode}[1] = $continent;

    return ($continent);
}

sub generateColors {
    if($htmlColors) {
        return \@colors;
    }
    my $cnt = shift;
    my $i = 0;
    my @outcolors=();
    my $max = 12;
    if($cnt > $max) {
        $max = $cnt;
    }
    my @outcolors = $color->analogous('ff0000', $cnt, $max);
    
    return \@outcolors;
}

sub readListFromJson {
    my $json = shift;
    my $list = {};
    if( -r $json ){
        open JSON, $json;
        flock(JSON, 1);
        local $/ = undef;
        $list = decode_json(<JSON>);
        close JSON;
     }
     return $list;
}
sub printOtherColor{
    my $fh = shift;
    print $fh <<'color';

serotype	autoagglutinable	#E9D0AF
serotype	inaba	#FF5D57
serotype	ogawa	#5F95E6
serotype	hikojima	#98CC6C
serotype	unknown	#B0B0B0

transmission_event	sporadic/local outbreak	#B0B0B0
transmission_event	T1	#1395BA
transmission_event	T2	#C03027
transmission_event	T3	#5BA793
transmission_event	T4	#105B78
transmission_event	T5	#ECAA37
transmission_event	T6	#0C3C55
transmission_event	T7	#D85027
transmission_event	T8	#A2B86C
transmission_event	T9	#EE8A29
transmission_event	T10	#117899
transmission_event	T11	#F16C23
transmission_event	T12	#EBC742
transmission_event	T13	#E56266

color
}

sub usage {
print<<'end';

Usage:   generateMetadata4Auspice.pl -n <virus name> -o <output directory> -aln <alignment file name>

end
exit;
}
