#FROM nextstrain/base
FROM continuumio/miniconda3

MAINTAINER Chienchi Lo <chienchi@lanl.gov>
LABEL version="0.01"
LABEL software="CholEDGE-nextstrain"
LABEL tags="bioinformatics"

# Tell systemd we're running in Docker!

ENV container docker
## inside docker provide host 0.0.0.0 instead of default localhost of auspice.
ENV HOST=0.0.0.0
ENV PATH="/choledge:${PATH}"

COPY CholEDGEdb.tgz /
COPY entrypoint /sbin/

RUN mkdir -p /auspice/data /data /choledge /db && chmod a+rx /sbin/entrypoint && \
    apt update -y && \
    apt install -y procps gcc && \
    conda install -y -c conda-forge -c bioconda -c defaults snippy perl-lwp-protocol-https FaQCs gubbins nodejs=10 cvxopt fasttree iqtree mafft pandas raxml vcftools git pip && \
    pip install python-dateutil==2.8.0 nextstrain-augur nextstrain-cli && \
    wget https://github.com/nextstrain/auspice/archive/v2.2.0.tar.gz && \
    tar -xzvf v2.2.0.tar.gz && cd auspice-2.2.0/ && \
    npm install && npm run build && npm link && \
    nextstrain check-setup --set-default && \
    cpanm Color::Mix && \
    mv /opt/conda/bin/raxmlHPC-PTHREADS-AVX2 /opt/conda/bin/raxmlHPC-PTHREADS-AVX2.bak && \
    cd / && wget https://github.com/broadinstitute/cromwell/releases/download/47/cromwell-47.jar && \
    tar xzf CholEDGEdb.tgz -C /db && \
    rm CholEDGEdb.tgz v2.2.0.tar.gz && \
    conda clean -y -a  && apt-get autoremove -y && apt-get clean 

    

    # install from archive
    #wget https://github.com/nextstrain/auspice/archive/v2.0.0.tar.gz && \
    #tar -xzvf v2.0.0.tar.gz && cd auspice-2.0.0/ && \
    #npm install && npm run build && npm link && \
#	&& \ rm -f ../v2.0.0.tar.gz

    # install from git
    #git checkout https://github.com/nextstrain/auspice.git && \
    #cd auspice && \
    #npm install && npm run build && npm link && \

COPY scripts/* /choledge/
COPY geo /choledge/
COPY snippy /opt/conda/bin/

# The host should bind mount the pathogen build dir into /data
WORKDIR /data

ENTRYPOINT ["/sbin/entrypoint"]

EXPOSE 4000

