## Repo
Git repo and example files: 
https://gitlab.com/chienchi/choledge

## Usage

1.  Launch Cromwell and Auspice Service

    ```docker run -d -p 8000:8000 -p 8001:4000 -v /path/to/data:/data -v /path/to/auspice_results:/auspice/data  bioedge/choledge ```

    * The -v /path/to/data:/data mounts the data directory to share files between the host and container.
    * The -v /path/to/auspice_results:/auspice/data mounts the output json files for Auspice Interactive Visualisation of Phylogenomic data
    * Cromwell Service Port in Container: 8000: The -p host:container bind the host port 8000 to container port 8000  inside the container. You can change the 8000 (front number) to fit your host system requirements.   
    * Auspice Service Port  in Container: 4000: The -p host:container bind the host port 8001 to container port 4000 inside the container. You can change the 8001 to fit your host system requirements.    


2. Browse the Cromwell Server REST API
    * open browser with URL: http://localhost:8000
    * How to [submit job](https://gitlab.com/chienchi/choledge/-/wikis/Usage)  
   
3.  Auspice Interactive Visualisation

    * open browser with URL: http://localhost:8001



### Paths to fastq and fasta data (working directory)
* The working directory inside container is **/data**

* You can download example dataset (fastq),  workflow file (CholEDGE.wdl),  example input.json file, and the example mappingFile.txt from git repo and [load wdl and input.json file through Cromwell Server REST API](https://gitlab.com/chienchi/choledge/-/wikis/Usage)  

* **Input**
 
    You will need to modify input.json and mappingFile.txt for your own data.

    * input.json is to define the cpu number, the project output directory **inside container** (/data/xxxx), the fastq/fasta files location and mappingfile location.
    ```
    {
      "core_snps.cpu" : 16,
      "core_snps.projdir": "/data/test",
      "core_snps.qc.mappingfile": "/data/mappingFile.txt",
      "core_snps.qc.indir": "/data"
    }
    ```

    * the mappingfile is a tab-delimited file with four columns, #SampleID, Files, Country and Years.
        * Files can be in a given subdirectory. In this example, the SRS3074319.1.fastq.gz file location will be /data/UG010/SRS3074319.1.fastq.gz.
    ```
    #SampleID	Files	Country	Years
    T1	UG010/SRS3074319.1.fastq.gz,UG010/SRS3074319.2.fastq.gz	Uganda	2015
    T2	UG020/SRS3074318.1.fastq.gz,UG020/SRS3074318.2.fastq.gz	Uganda	2015
    ```

* **Output**

    Below is the output directory structure
    
    * [QC](https://github.com/LANL-Bioinformatics/FaQCs): The quality controlled reads output.
    * [snippy](https://github.com/tseemann/snippy)  : The QC reads will run through snippy pipeline to get core genome and SNP info.
    * core:  The core alignment outputs.
    * [gubbins](https://sanger-pathogens.github.io/gubbins/): The recombination detection and mask
    * [auspice_workflow](https://nextstrain.org/docs/interpretation/auspice/) : The Nextstrain analysis workflow output. The final output .json file is in auspice_workflow/auspice/
    
   
    
### Note 
* Reads fastq can be gzip compressed.
* Required >70 GB memory.
* The example dataset took around 6 hours and 16 CPUs.
* This image is built on top of offical Ubuntu 18.04.3 LTS Base Image, and is officially supported on Docker Engine version 19.03.5.

## Contact Info
Chien-Chi Lo: chienchi@lanl.gov
